import doctest
from app.App import App

def add_numbers(a, b):
    """Returns the sum of these two numbers.

    >>> [add_numbers(a, a) for a in range(5)]
    [0, 2, 4, 6, 8]
    >>> add_numbers(2, 5)
    7
    """
    return App().add_numbers(a, b)

if __name__ == '__main__':
    doctest.testmod()
