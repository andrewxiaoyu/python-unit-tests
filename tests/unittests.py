import unittest
from app.App import App

class AppTests(unittest.TestCase):
    def setUp(self):
        self.App = App()

    def test_increment(self):
        self.App.value = 0
        self.App.increment()

        self.assertEqual(self.App.value, 1)

    def test_decrement(self):
        self.App.value = 0
        self.App.decrement()

        self.assertEqual(self.App.value, -1)
    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()
